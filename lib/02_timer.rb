class Timer
    def seconds()
        @seconds=0
    end
    def seconds=(time)
        @seconds=time
    end

    def time_string()
        @hours=@seconds/3600
        @minutes=(@seconds/60)%60
        @display=padded(@hours).to_s+":"+padded(@minutes).to_s+":"+padded(@seconds%60).to_s
    end
    def padded(time)
        pad="00"
        if time<10
            pad="0"+time.to_s
        else
            pad=time.to_s
        end
    end
end
