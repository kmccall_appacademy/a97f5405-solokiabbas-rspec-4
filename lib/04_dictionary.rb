class Dictionary

  def initialize
      @d={}
  end

  def add(temp)
      if temp.class == String
        @d={temp=> nil}
      else
        @d=@d.merge(temp)
      end

  end

  def entries()
      @d
  end

  def keywords
     @d.keys.sort
  end

  def include?(string)
      return @d.has_key?(string)
  end

  def find(string)
    @dic={}
      if @d.empty?
          return @dic
      end
      @dic=@d.select do |key,val|
        key[/#{string}/]
      end
      @dic
  end

  def printable
    @d.sort.map {|key,val| "[#{key}] \"#{val}\"\n"}.join.strip
  end

end

#obj=Dictionary.new
#obj.add("zebra" => "African land animal with stripes")
#obj.add("fish" => "aquatic animal")
#obj.add("apple" => "fruit")
#obj.printable
#puts %Q{[apple] "fruit"\n[fish] "aquatic animal"\n[zebra] "African land animal with stripes"}
