class Temperature
   class << self
        def from_celsius (temp)
            @temp={}
            @temp[:c]=temp
            @temp[:f]=(temp * 1.8 + 32).round(1)
            @temp
            #self
        end

        def from_fahrenheit (temp)
            @temp={}
            @temp[:f]=temp
            @temp[:c]=((temp - 32.0)/1.8).round(1)
            @temp
            #self
        end
        #refactored for second set of tests
        #def in_fahrenheit
        #    return @temp[:f]
        #end

        #def in_celsius
        #    return @temp[:c]
        #end
   end

        def in_fahrenheit
            return @temp[:f]
        end

        def in_celsius
            return @temp[:c]
        end

    def initialize (temper={})
        @temp={:c => temper[:c], :f => temper[:f]}
        if temper[:c] != nil
            @temp=self.class.from_celsius(temper[:c])
        else
            @temp=self.class.from_fahrenheit(temper[:f])
        end
    end

end

class Celsius < Temperature
    def initialize (temp)
        @temp=self.class.from_celsius(50)
    end
end
class Fahrenheit < Temperature
    def initialize (temp)
        @temp=self.class.from_fahrenheit(50)
    end
end
